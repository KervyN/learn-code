require 'prime'
def prime_product n
  primes  = Prime.each(n).to_a
  product = 0
  primes.each do |prime|
    next unless (n - prime).prime?
    sum = prime * (n - prime)
    product = sum if sum > product
  end
  product
end



def sieve_of_eratosthenes(sieve_size)
  # Initialize sieve array with true
  sieve = []
  sieve_size.times { sieve << true }

  # Set 0 and 1 to false because they aren't primes
  sieve[0] = false
  sieve[1] = false

  # Set index in the sieve array to false
  # if the corresponding index number is not a prime.
  # based on the sieve of Eratosthenes
  (2..(Math.sqrt(sieve_size).to_i + 1 )).each do |i|
    next if sieve[i] == false
    pointer = i * 2
    while pointer < sieve_size
      sieve[pointer] = false
      pointer += i
    end
  end

  # Build an array with prime numbers
  # based on the indexes of the sieve array
  primes = [2]
  (0..sieve_size).each do |i|
    primes << i if sieve[i] == true
  end
  primes.uniq
end
