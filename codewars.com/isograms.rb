def is_isogram(string)
  string.split(//).detect{ |e| string.downcase.count(e) > 1 } == nil ? true : false
end
