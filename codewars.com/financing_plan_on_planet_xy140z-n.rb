# http://www.codewars.com/kata/559ce00b70041bc7b600013d
def finance(n)
  sum = 0
  0.upto(n) { |i| sum += ((i*2) * ((i*2)+1)/2) - ((i-1) * i/2) }
  sum
end

finance 10000