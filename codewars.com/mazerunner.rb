maze = [[1,1,1,1,1,1,1],
        [1,0,0,0,0,0,3],
        [1,0,1,0,1,0,1],
        [0,0,1,0,0,0,1],
        [1,0,1,0,1,0,1],
        [1,0,0,0,0,0,1],
        [1,2,1,0,1,0,1]]

def maze_runner(maze, directions)
  start = maze.flat_map.with_index { |row, row_idx|
    row.each_index.select{ |i| 
      row[i] == 2 }.map{ |col_idx|
        [row_idx, col_idx] }
  }

  x = maze.length - 1

  pos = Array.new
  pos << start[0][0] << start[0][1]
  
  directions.each { |direction|
    case direction
    when 'N'
      pos[0] = pos[0] - 1
    when 'S'
      pos[0] = pos[0] + 1
    when 'E'
      pos[1] = pos[1] + 1
    when 'W'
      pos[1] = pos[1] - 1
    end
    if ((pos[0] or pos[1]) > x) or ((pos[0] or pos[1]) < 0) or (maze[pos[0]][pos[1]] == 1)
      break
    elsif (maze[pos[0]][pos[1]] == 3)
      break
    end
  }
  if ((pos[0] or pos[1]) > x) or ((pos[0] or pos[1]) < 0) or (maze[pos[0]][pos[1]] == 1)
    p 'Dead'
  else
    if maze[pos[0]][pos[1]] == 3
      p 'Finish'
    else
      p 'Lost'
    end
  end
end


maze_runner(maze,["S","N","N","N","N","E","E","E","E","E"])
maze_runner(maze,["N","N","N","N","N","E","E","E","E","E"])
maze_runner(maze,["N","N","N","N","N","E","E","E","E"])
