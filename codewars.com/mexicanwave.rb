def wave(str)
  if str.empty?
    s = Array.new
    s
  else
    result = Array.new
    starr = str.split('')
    starr.each_with_index { |x, index|
      next if x == ' '
      starr[index].upcase!
      result << starr.join
      starr[index].downcase!
    }
    result
  end
end
