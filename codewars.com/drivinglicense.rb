require 'date'

data = ['John', 'James', 'Smith', '01-Jan-2000', 'M']
license = []
license.push(data[2][0..4].ljust(5, '9').split(''))
license.push(Date.parse(data[3]).year.to_s[2])
license.push(Date.parse(data[3]).mon.to_s.rjust(2, '0').split(''))
license.push(Date.parse(data[3]).year.to_s[3])
# hier noch 12 und 13
license.push(%w[9 A A]).flatten
# hier noch sex auswerten
