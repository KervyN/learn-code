def get_order(order)
  menu = ["burger", "fries", "chicken", "pizza", "sandwich", "onionrings", "milkshake", "coke"]
  sorted = []
  menu.each do |k|
    sorted << order.scan(k)
  end
  sorted.flatten.inject('') do |full_name, name|
    "#{full_name} #{name.capitalize}".strip
  end

end
