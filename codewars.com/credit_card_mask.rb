def maskify(cc)
  puts ( cc.length >= 4 ? "#" * (cc.length - 4) : "" ) + cc.reverse[0..3].reverse
end
