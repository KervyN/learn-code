def countingBinomialCoefficient n,p
    m = p * (p+1) / 2
    
    t = m
    r = p
    
    while r <= n
        t *= m
        r *= p
    end

    o = 0
    row = 0
    index = 1
    mult = 1

    while t > 0
        if n + 1 - row >= r
            o += t * index * mult
            index += 1
            row += r
        else
            r /= p
            t /= m
            mult *= index
            index = 1
        end
    end
    puts o
end

countingBinomialCoefficient(5, 5)
countingBinomialCoefficient(999009999999992398749283713984739187348917234999, 17)